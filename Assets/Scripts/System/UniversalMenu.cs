﻿using System.Security.Cryptography;
using Notification;
using TMPro;
using UnityEngine;

namespace System {
	public class UniversalMenu : MonoBehaviour {
		public TMP_Text fpsCounter;

		public bool menuActive;

		public GameObject channels;
		public NotificationManager notificationManager;
		public GameObject notificationIndicatorsPrefab;
		public float indicatorSpacing;

		public GameObject dialBoardPrefab;
		public GameObject radioPrefab;
		public GameObject clockPrefab;
		
		private void Start() {
			notificationManager = FindObjectOfType<NotificationManager>();
		}

		private void Update() {
			if (menuActive) {
				fpsCounter.text = (1.0 / Time.deltaTime).ToString("F1") + " fps";
			}
		}

		public void WakeUpMenu() {
			notificationManager = FindObjectOfType<NotificationManager>();
			
			// Clear out notification channel indicators
			foreach (Transform child in channels.transform) {
				Destroy(child.gameObject);
			}

			// Place new channel indicators and render them
			float currentX = 0f;
			float currentY = 0f;
			int onThisLine = 0;
			foreach (VD_NotificationChannel channel in notificationManager.GetRegisteredChannels()) {
				GameObject indicator = Instantiate(notificationIndicatorsPrefab, channels.transform);
				indicator.transform.localPosition = new Vector3(currentX, currentY, 0);
				currentX += indicatorSpacing;
				indicator.GetComponent<ChannelControls>().channel = channel;
				indicator.GetComponent<ChannelControls>().Render();
				onThisLine++;
				if (onThisLine == 5) {
					currentY -= indicatorSpacing;
					currentX = 0f;
				}
			}
		}

		/// <summary>
		/// Event handler for the DialBoard spawn button. Spawns a new DialBoard set.
		/// </summary>
		public void SpawnDialBoard() {
			GameObject dialBoard = Instantiate(dialBoardPrefab);
			dialBoard.transform.position = new Vector3(0, 1, 0);
		}

		/// <summary>
		/// Event handler for the radio spawn button. Spawns a new radio.
		/// </summary>
		public void SpawnRadio() {
			GameObject radio = Instantiate(radioPrefab);
			radio.transform.position = new Vector3(0, 1, 0);
		}

		/// <summary>
		/// Event handler for the clock spawn button. Spawns a new clock.
		/// </summary>
		public void SpawnClock() {
			GameObject clock = Instantiate(clockPrefab);
			clock.transform.position = new Vector3(0, 1, 0);
		}
	}
}
