﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace System {
	/// <summary>
	/// Script to control the Universal Menu sliders.
	/// </summary>
	public class UniversalMenuSliders : MonoBehaviour {
		public LinearMapping quitLinearMapping;
		public LinearMapping deskHeightMapping;
		public LinearMapping deskWidthMapping;

		public TMP_Text deskHeightLabel;
		public TMP_Text deskWidthLabel;

		/// <summary>
		/// Called every frame. Checks the value of the quit slider, and exits the application when it reaches 97.5%.
		/// This also sets the labels on the desk height and width sliders.
		/// </summary>
		private void Update() {
			if (quitLinearMapping.value >= 0.975f) {
				Debug.Log("Application quit");
				Application.Quit();
			}

			deskHeightLabel.text = (deskHeightMapping.value * 3).ToString("F2");
			deskWidthLabel.text = (deskWidthMapping.value * 0.5).ToString("F2");
		}

		/// <summary>
		/// Event handler for when the apply button is clicked for the desk settings pane on the universal menu. Sets
		/// the values on the desk manager and calls for the desk to be rebuilt.
		/// </summary>
		public void ApplyDeskSettings() {
			DeskManager deskManager = FindObjectOfType<DeskManager>();
			
			// Delete existing walls and desks
			foreach (GameObject obj in deskManager.wallsAndDesks) {
				Destroy(obj);
			}
			
			// Apply settings and rebuild
			deskManager.wallsAndDesks = new List<GameObject>();
			deskManager.deskHeight = deskHeightMapping.value * 3;
			deskManager.deskLength = deskWidthMapping.value * 0.5f;
			deskManager.BuildDesk();
		}
	}
}
