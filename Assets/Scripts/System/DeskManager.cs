﻿using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace System {
	/// <summary>
	/// The <c>DeskManager</c> is responsible for the initial setup of the desk, and adding any additional items to it.
	/// This uses the SteamVR Chaperone to find the corners of the user's play space. Then, walls are built that go from
	/// each point to their adjacent neighbors.
	/// </summary>
	public class DeskManager : MonoBehaviour {
		public GameObject wallObject;
		public GameObject deskObject;

		public float deskLength;
		public float deskHeight;

		public Transform trashcan;
		public Transform clock;
		public Transform mailbox;

		public List<GameObject> wallsAndDesks;
	
		/// <summary>
		/// Called on start. Builds the desk.
		/// </summary>
		private void Start() {
			wallsAndDesks = new List<GameObject>();
			BuildDesk();
		}

		/// <summary>
		/// Retrieves the SteamVR Chaperone bounds and builds each of the walls. Once the walls are built, additional
		/// objects are placed.
		/// </summary>
		public void BuildDesk() {
			// Get the play area
			HmdQuad_t chaperonBounds = new HmdQuad_t();
			OpenVR.Chaperone.GetPlayAreaRect(ref chaperonBounds);

			// Build the walls
			BuildWall(0, chaperonBounds.vCorners0, chaperonBounds.vCorners1, false);
			BuildWall(90, chaperonBounds.vCorners1, chaperonBounds.vCorners2, true);
			BuildWall(180, chaperonBounds.vCorners2, chaperonBounds.vCorners3, false);
			BuildWall(270, chaperonBounds.vCorners3, chaperonBounds.vCorners0, true);
		
			// Place some essential items around the room
			trashcan.position = new Vector3(chaperonBounds.vCorners3.v0 - (deskLength * 5f), 0.255f, chaperonBounds.vCorners3.v2 - (deskLength * 3.5f));
			clock.position = new Vector3((chaperonBounds.vCorners0.v0 + chaperonBounds.vCorners1.v0) / 2, 2, chaperonBounds.vCorners0.v2 + 0.1f);
			mailbox.position = new Vector3(chaperonBounds.vCorners0.v0 - 0.4f, deskHeight, chaperonBounds.vCorners0.v2 + 0.25f);
		}

		/// <summary>
		/// Build the wall itself. Find the length between two corners, and the angle between them, then create the wall
		/// with the appropriate length and angle.
		/// </summary>
		/// <param name="yRotation">Any Y rotation offset, to make sure the wall is still facing the right way.</param>
		/// <param name="cornerA">The first corner.</param>
		/// <param name="cornerB">The second corner.</param>
		/// <param name="moveX">Whether we should offset in the X or Z direction for placement.</param>
		private void BuildWall(float yRotation, HmdVector3_t cornerA, HmdVector3_t cornerB, bool moveX) {
			float dx = Mathf.Abs(cornerA.v0 - cornerB.v0);
			float dz = Mathf.Abs(cornerA.v2 - cornerB.v2);
			float hyp = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dz, 2));
			float angle = Mathf.Atan(dz / dx);
		
			// Create the wall and set its scale
			GameObject wall1 = Instantiate(wallObject);
			wall1.transform.localScale = new Vector3(hyp, 4, 1);
			wall1.transform.rotation = Quaternion.Euler(new Vector3(90, angle + yRotation, 0));
		wallsAndDesks.Add(wall1);
			
			// Create the desk surface
			GameObject desk = Instantiate(deskObject);
			desk.transform.localScale = new Vector3(hyp, 1, deskLength);
			desk.transform.rotation = Quaternion.Euler(new Vector3(0, angle + yRotation, 0));
		wallsAndDesks.Add(desk);
			
			// Place the wall and desk, depending on whether it should be moved along the X or Z axis
			if (moveX) {
				wall1.transform.position = new Vector3(cornerA.v2 - (dx / 2), 0, 0);
				desk.transform.position =  new Vector3(cornerA.v2 - (dx / 2), deskHeight, 0);
			} else {
				wall1.transform.position = new Vector3(0, 0, cornerA.v2 - (dz / 2));
				desk.transform.position = new Vector3(0, deskHeight, cornerA.v2 - (dz / 2));
			}
		}
	}
}
