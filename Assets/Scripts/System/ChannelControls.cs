﻿using Notification;
using TMPro;
using UnityEngine;

namespace System {
    /// <summary>
    /// Behaviour class for the channel control spheres in the Universal Menu. This defines the behaviors the spheres
    /// should have.
    /// </summary>
    public class ChannelControls : MonoBehaviour {
        public NotificationManager notificationManager;
        public VD_NotificationChannel channel;

        public Color enabledColor;
        public Color disabledColor;

        public TMP_Text label;

        /// <summary>
        /// Render the channel controls. Set the label and the color.
        /// </summary>
        public void Render() {
            notificationManager = FindObjectOfType<NotificationManager>();
            
            label.text = channel.ChannelName + " @ " + channel.ApplicationName;
            if (notificationManager.IsActive(channel)) {
                GetComponent<MeshRenderer>().material.color = enabledColor;
            } else {
                GetComponent<MeshRenderer>().material.color = disabledColor;
            }
        }

        /// <summary>
        /// Event handler for when the sphere is clicked. Sends a request to the notification manager to toggle the
        /// channel's active status.
        /// </summary>
        public void Clicked() {
            notificationManager.ToggleChannelActive(channel);
            Render();
        }
    }
}
