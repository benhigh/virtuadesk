﻿using UnityEngine;

namespace Data.CoreData {
    /// <summary>
    /// <c>Audio</c> is a data class for pieces of audio data.This class contains both the actual audio itself, as an
    /// <c>AudioClip</c> object, and the title of the audio track as a string.
    /// </summary>
    public class Audio : Data {
        public AudioClip data;
        public string trackTitle;

        /// <summary>
        /// Constructor. Takes in the <c>AudioClip</c> data and the track title as arguments.
        /// </summary>
        /// <param name="data">The audio clip itself.</param>
        /// <param name="trackTitle">The title of the track.</param>
        public Audio(AudioClip data, string trackTitle) {
            this.data = data;
            this.trackTitle = trackTitle;
        }

        /// <summary>
        /// Returns the track title. When extending from <c>Audio</c>, consider overriding this method.
        /// </summary>
        /// <returns>The title of the track.</returns>
        public string GetTrackTitle() {
            return trackTitle;
        }
        
        public override string ToString() {
            return "VD_AUDIO: " + trackTitle;
        }
    }
}
