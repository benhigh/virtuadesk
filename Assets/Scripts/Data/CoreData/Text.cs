﻿using System;

namespace Data.CoreData {
    /// <summary>
    /// <c>Text</c> is a child of the <c>Data</c> class that provides basic textual data. This class only contains one
    /// field, <c>data</c>, which will contain the textual data this needs to represent.
    /// </summary>
    public class Text : Data {
        public string data;

        /// <summary>
        /// Constructor. Takes in and sets the <c>data</c> string.
        /// </summary>
        /// <param name="data">The text this data should represent.</param>
        public Text(string data) {
            this.data = data;
        }

        /// <summary>
        /// Method to get a small snippet of the data. When deriving from <c>Text</c>, developers should consider
        /// overriding this method.
        /// </summary>
        /// <returns>A snippet of the data in this class, with a maximum of 15 characters.</returns>
        public string GetSnippet() {
            return data.Substring(0, Math.Min(data.Length, 15));
        }
        
        public override string ToString() {
            return "VD_TEXT: " + data;
        }
    }
}
