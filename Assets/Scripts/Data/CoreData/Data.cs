﻿namespace Data.CoreData {
    /// <summary>
    /// Base <c>Data</c> class. As stated in the guidelines, developers should try to derive their data types from the
    /// existing child classes of data whenever possible, but data should always at least derive from this class. In and
    /// of itself, this class contains nothing special, just an override of <c>ToString()</c>, which developers should
    /// consider overriding when deriving from this class. 
    /// </summary>
    public abstract class Data {
        public override string ToString() {
            return "VirtuaDesk Data.";
        }
    }
}
