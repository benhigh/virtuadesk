﻿using System;
using Input;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

namespace Data {
    /// <summary>
    /// <c>VD_DataFacet</c> is a behavior component that should be put on any object that should act as a facet for data
    /// to be put into. This component provides the basic framework for receiving and holding data objects. This
    /// component contains some fields that should be adjusted on start on in the editor, such as <c>accepts</c>, which
    /// defines what types of data are accepted. Additionally, this component contains some events that will be invoked
    /// when certain events occur, such as valid or invalid data hovering over the facet. This component itself will
    /// perform any validation on hovering data that's necessary.
    ///
    /// This should never be the only component placed on a facet. When creating a facet, you will need to create a
    /// script that sets the <c>accepts</c> value, and provides any facet-specific behavior, such as updating labels and
    /// images.
    /// </summary>
    [RequireComponent(typeof(Interactable), typeof(Grabbable))]
    public class VD_DataFacet : MonoBehaviour {
        public enum GrabType {
            Removable, Cloneable
        }
        
        // Fields
        public Type accepts;
        public CoreData.Data data;
        public bool dataGrabbable;
        public bool acceptingData;
        public GrabType grabType;
        public GameObject spawnObject;

        // Events
        public UnityEvent onValidDataHover;
        public UnityEvent onInvalidDataHover;
        public UnityEvent onDataAway;
        public UnityEvent onDataDropped;
        public UnityEvent onDataClear;
        public UnityEvent onDataUpdate;
        
        /// <summary>
        /// Called when the facet is grabbed. if there's data in the facet, this will spawn a data object for the data
        /// in the facet, and attach it to the user's hand.
        /// </summary>
        /// <param name="svrHand">The SteamVR hand that grabbed the object. Used to attach the spawned data object to
        /// the user's hand.</param>
        public void Grabbed(Hand svrHand) {
            if (dataGrabbable && data != null) {
                // Create the data object, set its data, and attach it to the hand
                GameObject newData = Instantiate(spawnObject);
                newData.GetComponent<VD_DataObject>().data = data;
                newData.GetComponent<VD_DataObject>().onSpawn.Invoke();
                svrHand.AttachObject(newData, GrabTypes.Grip);
                
                // Inform the hand it's grabbing something else
                GrabControls grabControls = svrHand.GetComponent<GrabControls>();
                grabControls.Drop();
                grabControls.Grab();
                grabControls.grabbableCollidingWith = newData.GetComponent<Grabbable>();
                
                // Check if we need to clear this facet's data
                if (grabType == GrabType.Removable) {
                    data = null;
                    onDataClear.Invoke();
                }
            }
        }

        /// <summary>
        /// Called when data is hovering over the facet. Makes sure the facet is accepting data and the data is of the
        /// correct type for this facet. Will return true if the data is acceptable, or false otherwise.
        /// </summary>
        /// <param name="dataObject">The <c>VD_DataObject</c> hovering over the facet.</param>
        /// <returns>Whether or not this facet will accept the data.</returns>
        public bool DataHover(VD_DataObject dataObject) {
            if (acceptingData && dataObject.data.GetType().IsEquivalentTo(accepts)) {
                onValidDataHover.Invoke();
                return true;
            }

            onInvalidDataHover.Invoke();
            return false;
        }

        /// <summary>
        /// Called when data is pulled away from the facet. Simply calls the appropriate event.
        /// </summary>
        public void DataAway() {
            onDataAway.Invoke();
        }

        /// <summary>
        /// Called when valid data is dropped into the facet. Sets the data, destroys the data object, and calls the
        /// appropriate events.
        /// </summary>
        /// <param name="dataObject">The <c>VD_DataObject</c> hovering and dropped onto the facet.</param>
        public void DataDropped(VD_DataObject dataObject) {
            data = dataObject.data;
            onDataDropped.Invoke();
            Destroy(dataObject.gameObject);
            DataAway();
        }

        /// <summary>
        /// Called when the data in the facet should be repopulated with new data. Sets the new data and calls the
        /// appropriate event.
        /// </summary>
        /// <param name="newData">The new data to be placed into the facet.</param>
        public void UpdateData(CoreData.Data newData) {
            data = newData;
            onDataUpdate.Invoke();
        }
    }
}
