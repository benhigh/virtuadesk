﻿using System;
using Data.CoreData;
using TMPro;
using UnityEngine;

namespace Data.CoreDataBehaviors {
	/// <summary>
	/// <c>TextBehavior</c> contains event handlers for text objects.
	/// </summary>
	public class TextBehavior : MonoBehaviour {
		public TMP_Text tmpText;

		/// <summary>
		/// Called on spawn. Sets the snippet label.
		/// </summary>
		public void Spawn() {
			VD_DataObject dataObject = GetComponent<VD_DataObject>();
			tmpText.text = (dataObject.data as Text)?.GetSnippet();
		}

		/// <summary>
		/// Called when hovering over a valid facet. Sets the color of the text object to green.
		/// </summary>
		public void HoverValid() {
			GetComponent<Renderer>().material.color = Color.green;
		}

		/// <summary>
		/// Called when pulled away from a facet. Sets the color of the text object back to white.
		/// </summary>
		public void Away() {
			GetComponent<Renderer>().material.color = Color.white;
		}
	}
}
