﻿using System;
using Data.CoreData;
using TMPro;
using UnityEngine;

namespace Data.CoreDataBehaviors {
    /// <summary>
    /// <c>AudioFacetBehavior</c> provides event handlers for the audio data facet. This also sets the required fields
    /// for the <c>VD_DataFacet</c> attached to this object.
    /// </summary>
    public class AudioFacetBehavior : MonoBehaviour {
        public TMP_Text trackLabel;
        public TMP_Text acceptableDataLabel;

        /// <summary>
        /// Called on start. Sets the data to null and accept type to accept <c>Audio</c> data.
        /// </summary>
        private void Start() {
            GetComponent<VD_DataFacet>().data = null;
            GetComponent<VD_DataFacet>().accepts = typeof(Audio);
        }

        /// <summary>
        /// Called when valid audio is hovered over this facet. Simply displays the acceptable data label.
        /// </summary>
        public void ValidHover() {
            acceptableDataLabel.gameObject.SetActive(true);
        }

        /// <summary>
        /// Called the audio is taken away from this facet. Removes the acceptable data label.
        /// </summary>
        public void Away() {
            acceptableDataLabel.gameObject.SetActive(false);
        }

        /// <summary>
        /// Called when audio is dropped into the facet. Displays the audio track name, and tells the facet to stop
        /// accepting more data.
        /// </summary>
        public void Dropped() {
            Audio audioData = GetComponent<VD_DataFacet>().data as Audio;
            trackLabel.text = audioData?.trackTitle;
            trackLabel.color = Color.white;
            GetComponent<VD_DataFacet>().acceptingData = false;
        }

        /// <summary>
        /// Called when the data in the facet is removed. Changes the track name to a red "None" label and opens the
        /// facet to accepting data.
        /// </summary>
        public void Cleared() {
            trackLabel.text = "None";
            trackLabel.color = Color.red;
            GetComponent<VD_DataFacet>().acceptingData = true;
        }

        /// <summary>
        /// Called when the data in the facet changes. Changes the track name label, and ensures the track label is
        /// white. 
        /// </summary>
        public void Updated() {
            Audio audioData = GetComponent<VD_DataFacet>().data as Audio;
            trackLabel.text = audioData?.trackTitle;
            trackLabel.color = Color.white;
        }
    }
}
