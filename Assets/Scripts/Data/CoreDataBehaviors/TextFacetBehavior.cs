﻿using System;
using Data.CoreData;
using TMPro;
using UnityEngine;

namespace Data.CoreDataBehaviors {
	/// <summary>
	/// <c>TextFacetBehavior</c> defines event handlers for text facets.
	/// </summary>
	public class TextFacetBehavior : MonoBehaviour {
		public TMP_Text tmpText;
		public string defaultText;

		/// <summary>
		/// On start, set the acceptable data type as <c>Text</c>, and create any default data that should go into the
		/// facet from the start.
		/// </summary>
		private void Start() {
			GetComponent<VD_DataFacet>().data = new Text(defaultText);
			GetComponent<VD_DataFacet>().accepts = typeof(Text);
			Dropped();
		}

		/// <summary>
		/// Called when valid data hovers over the facet. Set the facet to green.
		/// </summary>
		public void ValidHover() {
			GetComponent<Renderer>().material.color = Color.green;
		}

		/// <summary>
		/// Called when invalid data hovers over the facet. Set the facet to red.
		/// </summary>
		public void InvalidHover() {
			GetComponent<Renderer>().material.color = Color.red;
		}

		/// <summary>
		/// Called when data is no longer hovering over the facet. Se the color back to white.
		/// </summary>
		public void Away() {
			GetComponent<Renderer>().material.color = Color.white;
		}

		/// <summary>
		/// Called when data is dropped into the facet. Update the label and disallow any more data to be dropped in.
		/// </summary>
		public void Dropped() {
			Text text = GetComponent<VD_DataFacet>().data as Text;
			tmpText.color = Color.blue;
			tmpText.text = text?.data;
			GetComponent<VD_DataFacet>().acceptingData = false;
		}

		/// <summary>
		/// Called when data is pulled out of the facet. Set the label on the facet to say that nothing is in the facet,
		/// and open the facet to new data.
		/// </summary>
		public void Cleared() {
			tmpText.color = Color.red;
			tmpText.text = "[nothing]";
			GetComponent<VD_DataFacet>().acceptingData = true;
		}

		/// <summary>
		/// Called when the data in the facet is changed. Set the label text to reflect the new value of the data.
		/// </summary>
		public void Updated() {
			Text text = GetComponent<VD_DataFacet>().data as Text;
			tmpText.text = text?.data;
			tmpText.color = Color.blue;
		}
	}
}
