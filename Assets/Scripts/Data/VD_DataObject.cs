﻿using System;
using System.Linq;
using Input;
using UnityEngine;
using UnityEngine.Events;

namespace Data {
    /// <summary>
    /// <c>VD_DataObject</c> is a behaviour component that should be applied to any "physical" object that represents
    /// data of any form. This component will provide the fundamental requirements for data objects, including holding
    /// the data itself. Additionally, this component provides some <c>UnityEvent</c>s that can be used for populating
    /// any object-specific fields (such as a text label) on spawn, and when colliding with both a valid and invalid
    /// facet.
    ///
    /// In nearly every case, this should not be the only component applied to a data object. You should always bundle
    /// this component with a custom component that manages any text or image labels to describe your pieces of data, as
    /// per the guidelines on data objects. That said, this component should always be on a data object, otherwise it
    /// will not be correctly recognized by facets.
    /// </summary>
    [RequireComponent(typeof(Grabbable))]
    public class VD_DataObject : MonoBehaviour {
        public CoreData.Data data;
        public UnityEvent onSpawn;
        
        public UnityEvent onValidFacetHover;
        public UnityEvent onValidFacetAway;

        public VD_DataFacet collidingFacet;

        public VD_DataFacet[] previouslyContacting;
        
        private Grabbable grabbable;

        /// <summary>
        /// This method simply gets the <c>Grabbable</c> component from the object on start. 
        /// </summary>
        private void Start() {
            grabbable = GetComponent<Grabbable>();
        }

        /// <summary>
        /// Event handler for when the piece of data is dropped. This will simply update any colliding facets to let
        /// them know the data has been dropped and can therefore be retrieved (if the facet supports the data, of
        /// course).
        /// </summary>
        public void Dropped() {
            grabbable.grabbedBy.holdingData = false;
            grabbable.grabbedBy.dataObjectHolding = null;
            
            if (collidingFacet != null) {
                collidingFacet.DataDropped(this);
            }
        }

        /// <summary>
        /// Event handler for when the data is grabbed. This will inform the grabbing hand that it's holding data.
        /// </summary>
        /// <param name="by">The <c>GrabControls</c> for the hand holding the data object.</param>
        public void Grabbed(GrabControls by) {
            by.holdingData = true;
            by.dataObjectHolding = this;
        }

        /// <summary>
        /// Called every frame. Checks for any colliding facets and informs them either of new hovers or new removals.
        /// </summary>
        public void Update() {
            // Find all of our colliding facets
            Collider[] collidingWith = Physics.OverlapSphere(transform.position, 0.1f);
            collidingWith = Array.FindAll(collidingWith, o => o.gameObject.GetComponent<VD_DataFacet>() != null);
            VD_DataFacet[] facetsColliding = collidingWith.ToList().ConvertAll<VD_DataFacet>(o => o.GetComponent<VD_DataFacet>()).ToArray();
            
            if (collidingFacet != null) {
                // If we're no longer colliding with our preferred facet, then let it know that's happening
                if (!facetsColliding.Contains(collidingFacet)) {
                    onValidFacetAway.Invoke();
                    collidingFacet.DataAway();
                    collidingFacet = null;
                }
            } else {
                // Set the first valid facet we're colliding with as our preferred facet
                foreach (VD_DataFacet facet in facetsColliding) {
                    bool valid = facet.DataHover(this);
                    if (valid) {
                        onValidFacetHover.Invoke();
                        collidingFacet = facet;
                        break;
                    }
                }
            }

            // Inform all facets we were contacting before, but are no longer, that we're not contacting them anymore-- valid or not
            foreach (VD_DataFacet facet in previouslyContacting) {
                if (!facetsColliding.Contains(facet)) {
                    facet.DataAway();
                }
            }
            previouslyContacting = facetsColliding;
        }
    }
}
