﻿using System;
using Data;
using Data.CoreData;
using Data.CoreDataBehaviors;
using Input;
using Notification;
using TMPro;
using UnityEngine;

namespace Examples {
    /// <summary>
    /// Behavior for the custom notification sender. The custom notification sender is a state-based object, which
    /// allows the user to test sending a notification. The object contains 4 facets which are displayed 2 at a a time:
    /// channel name, application name, notification subject, and notification label. The state is progressed when the
    /// trigger is clicked, assuming the facets all have some data in them. Once the notification has been sent, the
    /// canvas will display the status of the notification.
    /// </summary>
    public class CustomNotificationTester : MonoBehaviour {
        enum States {
            NO_CHANNEL,
            CHANEL_MADE,
            NOTIFICATION_SENT
        }
        
        private VD_InputReceiver inputReceiver;
        private VD_NotificationSender notificationSender;

        private States currentState = States.NO_CHANNEL;
        
        private VD_NotificationChannel channel;
        private VD_Notification lastNotification;

        public Canvas channelCreateCanvas;
        public VD_DataFacet channelNameFacet;
        public VD_DataFacet applicationNameFacet;

        public Canvas notificationSendCanvas;
        public VD_DataFacet notificationSubjectFacet;
        public VD_DataFacet notificationLabelFacet;
        
        public TMP_Text textLabel;
        
        /// <summary>
        /// Called on start. Get our input receiver and notification sender.
        /// </summary>
        private void Start() {
            inputReceiver = GetComponent<VD_InputReceiver>();
            notificationSender = GetComponent<VD_NotificationSender>();
        }

        /// <summary>
        /// Called every frame. Gets input from the input receiver, progresses the state of the component, and sends and
        /// monitors notifications.
        /// </summary>
        private void Update() {
            // Always make sure the input receiver is active.
            if (inputReceiver.isActive) {
                // Trigger to progress state.
                VD_InputTriggerState triggerState = inputReceiver.GetInput().TriggerState;

                if (triggerState.ClickedDown) {
                    if (currentState == States.NO_CHANNEL) {
                        // Get the data in the channel setup facets.
                        Text channelNameText = channelNameFacet.data as Text;
                        Text applicationNameText = applicationNameFacet.data as Text;

                        // Make sure both facets have some data in them. If so, progress to the next state.
                        if (channelNameText != null && applicationNameText != null) {
                            channel = notificationSender.CreateChannel(channelNameText.data, applicationNameText.data);
                            currentState = States.CHANEL_MADE;
                            textLabel.text = "Press trigger to send notification.";
                            channelCreateCanvas.gameObject.SetActive(false);
                            notificationSendCanvas.gameObject.SetActive(true);
                        } else {
                            textLabel.text = "Please make sure all text facets are filled.";
                        }
                    } else if (currentState == States.CHANEL_MADE) {
                        // Get the data in the notification info facets.
                        Text subjectText = notificationSubjectFacet.data as Text;
                        Text labelText = notificationLabelFacet.data as Text;

                        // Make sure both facets have some data in them. If so, send the notification and switch to the
                        // notification monitoring state.
                        if (subjectText != null && labelText != null) {
                            lastNotification =
                                notificationSender.CreateNotification(channel, subjectText.data, labelText.data, new Text("This is some example data."));
                            currentState = States.NOTIFICATION_SENT;
                            notificationSendCanvas.gameObject.SetActive(false);
                        } else {
                            textLabel.text = "Please make sure all text facets are filled.";
                        }
                    } else {
                        // If we're switching back to the channel creation state, set the label and canvas.
                        textLabel.text = "Press trigger to create channel.";
                        channelCreateCanvas.gameObject.SetActive(true);
                        currentState = States.NO_CHANNEL;
                    }
                }
            }

            // If we're watching the notification status, update the label regardless of if the object is grabbed.
            if (currentState == States.NOTIFICATION_SENT) {
                if (!lastNotification.sent) {
                    textLabel.text = "Notification blocked.";
                } else if (lastNotification.destroyed) {
                    textLabel.text = "Notification destroyed.";
                } else if (lastNotification.opened) {
                    textLabel.text = "Notification opened.";
                } else {
                    textLabel.text = "Notification sent.";
                }
            }
        }
    }
}
