﻿using UnityEngine;

namespace Examples {
    /// <summary>
    /// Behavior for the radio buttons. Contains an event handler for when the input receiver attached to the buttons is
    /// clicked, which will call the correct action on the radio for the button type, which is defined as
    /// <c>buttonType</c>.
    /// </summary>
    public class RadioButton : MonoBehaviour {
        public enum RadioButtonType {
            PlayPause, Stop
        }

        public RadioButtonType buttonType;
        public Radio targetRadio;

        /// <summary>
        /// Event handler for when the button is clicked. Calls the correct method on the radio.
        /// </summary>
        public void Clicked() {
            if (buttonType == RadioButtonType.PlayPause) {
                targetRadio.StartPauseAudio();
            } else {
                targetRadio.StopAudio();
            }
        }
    }
}
