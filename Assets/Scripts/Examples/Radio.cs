﻿using Data;
using Data.CoreData;
using UnityEngine;

namespace Examples {
    /// <summary>
    /// Behavior for the radio example object. This object contains an audio facet and two clickable buttons. This is a
    /// practical multimedia player that tests out audio data, as well as clickable objects.
    /// </summary>
    public class Radio : MonoBehaviour {
        public AudioSource audioSource;
        public VD_DataFacet dataFacet;

        /// <summary>
        /// Called by the start/pause button. Either plays audio in the facet, or pauses it.
        /// </summary>
        public void StartPauseAudio() {
            // Play audio if there's audio in the facet. Otherwise, pause.
            if (dataFacet.data is Audio audioData) {
                if (audioSource.isPlaying) {
                    audioSource.Pause();
                } else {
                    audioSource.clip = audioData.data;
                    audioSource.Play();
                }
            }
        }

        /// <summary>
        /// Called by the stop button. Stops the audio completely.
        /// </summary>
        public void StopAudio() {
            audioSource.Stop();
        }
    }
}
