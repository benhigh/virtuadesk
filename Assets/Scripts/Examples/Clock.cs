﻿using System;
using Input;
using TMPro;
using UnityEngine;

namespace Examples {
	/// <summary>
	/// Behavior for the example clock. The clock can show either 12 or 24 hour time, and which one it displays is
	/// controlled by a click of the trigger. This serves as a practical example of using the input API.
	/// </summary>
	public class Clock : MonoBehaviour {
		public TMP_Text timeText;
		public GameObject tipPanel;
		public RectTransform tipPanelTimeoutIndicator;
		public bool twelveHour;
	
		private VD_InputReceiver inputReceiver;

		private float tipPanelShownFor;
		public float tipPanelSeconds;

		/// <summary>
		/// Called on start. Gets our input manager.
		/// </summary>
		private void Start() {
			inputReceiver = GetComponent<VD_InputReceiver>();
			tipPanelShownFor = 0;
		}

		/// <summary>
		/// Called every frame. Waits for the object to be picked up, then checks for a trigger press. When the trigger
		/// is pressed down, the time format is changed. The tip panel timeout is also controlled every frame.
		/// </summary>
		private void Update() {
			// Check for time format change (click trigger)
			if (inputReceiver.isActive) {
				VD_InputsState inputsState = inputReceiver.GetInput();
				if (inputsState.TriggerState.ClickedDown) {
					twelveHour = !twelveHour;
				}
			}

			// Set the time
			DateTime currentTime = DateTime.Now;
			if (twelveHour) {
				timeText.text = currentTime.ToString("h:mm tt");
			} else {
				timeText.text = currentTime.ToString("HH:mm");
			}
		
			// Handle the tip panel timeout
			if (tipPanel.activeSelf) {
				tipPanelShownFor += Time.deltaTime;
				tipPanelTimeoutIndicator.localScale = new Vector3(tipPanelShownFor / tipPanelSeconds, 1, 1);

				if (tipPanelShownFor >= tipPanelSeconds) {
					HideTipPanel();
				}
			}
		}

		/// <summary>
		/// Event handler for when the object is picked up. THis will show the tip panel.
		/// </summary>
		public void ShowTipPanel() {
			tipPanel.SetActive(true);
			tipPanelShownFor = 0;
		}

		/// <summary>
		/// Event handler for when the object is dropped. This will hide the tip panel.
		/// </summary>
		public void HideTipPanel() {
			tipPanel.SetActive(false);
		}
	}
}
