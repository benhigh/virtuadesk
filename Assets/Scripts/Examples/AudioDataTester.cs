﻿using System;
using Data;
using Data.CoreData;
using Data.CoreDataBehaviors;
using UnityEngine;

namespace Examples {
    /// <summary>
    /// Behavior for the audio data tester. On start, this simply spawns a single instance of audio data into the scene.
    /// </summary>
    public class AudioDataTester : MonoBehaviour {
        public AudioClip clip;
        public string clipTitle;
        public GameObject audioPrefab;
        
        /// <summary>
        /// Called on start. Creates an <c>Audio</c> object and drops the data object into the scene.
        /// </summary>
        private void Start() {
            Audio a = new Audio(clip, clipTitle);
            
            GameObject obj = Instantiate(audioPrefab);
            obj.GetComponent<VD_DataObject>().data = a;
            obj.GetComponent<AudioBehavior>().Spawn();
            obj.transform.position = new Vector3(-0.122f, 2, 0.3f);
        }
    }
}
