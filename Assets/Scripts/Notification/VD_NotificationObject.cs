﻿using Data;
using Input;
using TMPro;
using UnityEngine;

namespace Notification {
    /// <summary>
    /// Behavior for the notification object. Handles populating the notification object with data, and keeping track of
    /// its UI state. When the trigger is pulled while holding a notification, the notification is opened. This script
    /// will update the <c>VD_Notification</c> object to reflect the current status of the notification object.
    /// </summary>
    public class VD_NotificationObject : MonoBehaviour {
        private VD_Notification notification;

        public TMP_Text subjectLine;
        public TMP_Text fromText;
        public TMP_Text headerLabel;
        public TMP_Text dateLabel;
        public TMP_Text triggerLabel;
        public TMP_Text labelText;
        public VD_DataFacet dataFacet;
    
        private VD_InputReceiver inputReceiver;

        /// <summary>
        /// Called on start. Get our input receiver.
        /// </summary>
        private void Start() {
            inputReceiver = GetComponent<VD_InputReceiver>();
        }

        /// <summary>
        /// Called every frame. Gets if the trigger is down, and opens the notification if it is.
        /// </summary>
        private void Update() {
            // Always check if the input receiver is active first.
            if (inputReceiver.isActive) {
                VD_InputTriggerState triggerState = inputReceiver.GetInput().TriggerState;
                
                if (triggerState.ClickedDown) {
                    // If the trigger has been clicked, clear our the outer notification labels.
                    subjectLine.gameObject.SetActive(false);
                    fromText.gameObject.SetActive(false);
                    headerLabel.gameObject.SetActive(false);
                    triggerLabel.gameObject.SetActive(false);
                
                    // Show the inner notification labels.
                    labelText.gameObject.SetActive(true);
                    if (notification.data != null) {
                        dataFacet.gameObject.SetActive(true);
                    }

                    // Set the notification as open.
                    notification.opened = true;
                }
            }
        }

        /// <summary>
        /// Called by <c>VD_Component</c> when the notification is thrown away. Just updates the notification object to
        /// show that the notification has been thrown away.
        /// </summary>
        public void ThrownAway() {
            notification.destroyed = true;
        }
        
        /// <summary>
        /// Populate the notification's labels with data from the notification's object.
        /// </summary>
        /// <param name="targetNotification">The <c>VD_Notification</c> this object should represent.</param>
        public void Populate(VD_Notification targetNotification) {
            notification = targetNotification;
            subjectLine.text = targetNotification.subject;
            fromText.text = targetNotification.channel.ApplicationName;
            dateLabel.text = targetNotification.created.ToShortDateString() + " " +
                             targetNotification.created.ToShortTimeString();
            labelText.text = targetNotification.label;
            dataFacet.data = targetNotification.data;
        }
    }
}
