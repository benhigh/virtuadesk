﻿using System;

namespace Notification {
	/// <summary>
	/// Data class for notifications. Simply holds the data for the notifications.
	/// </summary>
	public class VD_Notification {
		public string label;
		public string subject;
		public VD_NotificationChannel channel;
		public Data.CoreData.Data data;
		public bool sent;
		public bool opened;
		public bool destroyed;
		public DateTime created;

		public VD_Notification(string label, string subject, VD_NotificationChannel channel, Data.CoreData.Data data) {
			this.label = label;
			this.subject = subject;
			this.channel = channel;
			this.data = data;
			sent = true;
			opened = false;
			destroyed = false;
			created = DateTime.Now;
		}
	}
}
