﻿using System;
using UnityEngine;

namespace Notification {
	/// <summary>
	/// <c>VD_NotificationSender</c> allows the component to which it is attached to interface with the system level
	/// notification manager. This script should be attached to one or more components when notifications should be sent
	/// by the application. There is no limit to how many components can have <c>VD_NotificationSender</c> scripts, but
	/// applications should be designed to have as few as possible.
	/// </summary>
	public class VD_NotificationSender : MonoBehaviour {
		private NotificationManager notificationManager;

		/// <summary>
		/// Called on start. Find our system level notification manager.
		/// </summary>
		private void Start() {
			notificationManager = FindObjectOfType<NotificationManager>();
		}

		/// <summary>
		/// Create a channel. Ask the system level notification manager to create and register a channel.
		/// </summary>
		/// <param name="channelName">The channel name.</param>
		/// <param name="applicationName">The name of the application.</param>
		/// <returns>A <c>VD_NotificationChannel</c> object for this new channel.</returns>
		public VD_NotificationChannel CreateChannel(string channelName, string applicationName) {
			return notificationManager.CreateChannel(channelName, applicationName);
		}

		/// <summary>
		/// Send a notification. Ask the system level notification manager to send a notification.
		/// </summary>
		/// <param name="channel">The <c>VD_NotificationChannel</c> to send this notification through.</param>
		/// <param name="subject">The outer subject of the notification.</param>
		/// <param name="label">The inner label of the notification.</param>
		/// <param name="contents">Any data that we should send with the notification.</param>
		/// <returns>A <c>VD_Notification</c> object if the notification was sent. Otherwise, null.</returns>
		public VD_Notification CreateNotification(VD_NotificationChannel channel, string subject, string label,
			Data.CoreData.Data contents) {
			return notificationManager.SendNotification(channel, subject, label, contents);
		}
	}
}
