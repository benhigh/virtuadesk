﻿using System;
using System.Collections.Generic;
using Input;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DialBoard {
	/// <summary>
	/// <c>Dial</c> is the behavior component for each individual dial object in the DialBoard. This class manages the
	/// behaviors of the dials, allowing them to work independently of each other.
	/// </summary>
	public class Dial : MonoBehaviour {
		private List<string> layouts;
		private int currentLayout = 1;

		public DialBoardManager manager;
		public DialBoardManager.Hand thisHand;

		public float entryThreshold = 0.5f;
		
		public float innerSphereMaxMovement = 0.0065f;
		public Transform innerSphere;

		public bool upperCase;
		public bool upperCaseLock;

		public Color normalLabelColor;
		public Color hoveredLabelColor;

		public Image capsIndicator;
		
		public Sprite upperCaseDisabledImage;
		public Sprite upperCaseEnabledImage;

		public Color capsLockDisabledColor;
		public Color capsLockEnabledColor;

		public VD_InputReceiver inputReceiver;

		public List<TMP_Text> characterLabels;
		
		/// <summary>
		/// On start, get the layouts for this hand from the <c>DialBoardManager</c>, and set the current layout to the
		/// first layout.
		/// </summary>
		private void Start() {
			layouts = manager.getLayouts(thisHand);
			changeLayout(1);
		}

		/// <summary>
		/// Change the layout of this dial. Gets the characters for the new layout, and updates the labels around the
		/// dial to reflect the new layout.
		/// </summary>
		/// <param name="newLayout">The layout number for the new layout we're moving to.</param>
		private void changeLayout(int newLayout) {
			string newLayoutValues = layouts[newLayout - 1];

			for (int i = 0; i < newLayoutValues.Length; i++) {
				if (newLayoutValues[i].ToString() == " ") {
					characterLabels[i].text = "[space]";
				} else {
					if (upperCase) {
						characterLabels[i].text = newLayoutValues.ToUpper()[i].ToString();
					} else {
						characterLabels[i].text = newLayoutValues[i].ToString();
					}
				}
			}
		}

		/// <summary>
		/// Updates the caps indicator to the correct image and color. The indicator should be filled when in upper case
		/// or caps lock, and should be red when in caps lock.
		/// </summary>
		private void updateCapsIndicator() {
			// Update the indicator image
			if (upperCase) {
				capsIndicator.sprite = upperCaseEnabledImage;
			} else {
				capsIndicator.sprite = upperCaseDisabledImage;
			}
            						
			// Update the indicator color
			if (upperCaseLock) {
				capsIndicator.color = capsLockEnabledColor;
			} else {
				capsIndicator.color = capsLockDisabledColor;
			}
		}
		
		/// <summary>
		/// Called each frame. This gets the input from the <c>InputReceiver</c>, and responds to events where the user
		/// either clicks down the trigger or the touchpad.
		/// </summary>
		private void Update() {
			// Check if the trigger is being clicked down
			if (inputReceiver.isActive) {
				VD_InputsState inputsState = inputReceiver.GetInput();
				Vector2 touchpadLocation = inputsState.TouchpadState.Value;

				// Update inner sphere location
				innerSphere.localPosition = new Vector3(-touchpadLocation.x * innerSphereMaxMovement, touchpadLocation.y * innerSphereMaxMovement, 0.00475f);
				
				// Check if we're changing layout
				if (inputsState.TriggerState.ClickedDown) {
					if (currentLayout == layouts.Count) {
						currentLayout = 1;
						changeLayout(1);
					} else {
						changeLayout(++currentLayout);
					}
				}
				
				// Clear label styles
				foreach (TMP_Text characterLabel in characterLabels) {
					characterLabel.color = normalLabelColor;
					characterLabel.fontWeight = FontWeight.Regular;
				}
				
				// Determine if we're in range of a character
				bool inRangeOfCharacter = Mathf.Sqrt((touchpadLocation.x * touchpadLocation.x) + (touchpadLocation.y * touchpadLocation.y)) > entryThreshold;
				DialBoardManager.Heading heading = DialBoardManager.Heading.North;
				if (inRangeOfCharacter) {
					// Get the angle and determine heading
					double angle = (180 / Math.PI) *
					               (Mathf.Atan2(touchpadLocation.y, touchpadLocation.x) - Mathf.Atan2(1, 0)) * -1;
					if (angle > -30 && angle <= 30) {
						heading = DialBoardManager.Heading.North;
					} else if (angle > 30 && angle <= 60) {
						heading = DialBoardManager.Heading.Northeast;
					} else if (angle > 60 && angle <= 120) {
						heading = DialBoardManager.Heading.East;
					} else if (angle > 120 && angle <= 150) {
						heading = DialBoardManager.Heading.Southeast;
					} else if (angle > 150 && angle <= 210) {
						heading = DialBoardManager.Heading.South;
					} else if (angle > 210 && angle <= 240) {
						heading = DialBoardManager.Heading.Southwest;
					} else if (angle > 240 || angle <= -60) {
						heading = DialBoardManager.Heading.West;
					} else {
						heading = DialBoardManager.Heading.Northwest;
					}

					// Update the style of the active label
					characterLabels[(int) heading].color = hoveredLabelColor;
					characterLabels[(int) heading].fontWeight = FontWeight.Bold;
				}

				if (inputsState.TouchpadState.ClickDown) {
					// Check if we're going uppercase or entering a key
					if (inRangeOfCharacter) {
						// Send the character
						manager.addCharacter(thisHand, currentLayout, heading, upperCase);
						
						// Change upper case if needed
						if (!upperCaseLock) {
							upperCase = false;
						}
						
						// Update the labels and caps indicator
						changeLayout(currentLayout);
						updateCapsIndicator();
					} else {
						// Check if we're disabling caps lock, enabling caps lock, or switching to upper case
						if (upperCaseLock) {
							upperCase = false;
							upperCaseLock = false;
						} else if (upperCase) {
							upperCaseLock = true;
						} else {
							upperCase = true;
						}

						updateCapsIndicator();
						
						// Update the labels
						changeLayout(currentLayout);
					}
				}
			} else {
				// Default the inner sphere to center
				innerSphere.localPosition = new Vector3(0, 0, 0.00475f);
				
				// Clear label styles
				foreach (TMP_Text characterLabel in characterLabels) {
					characterLabel.color = normalLabelColor;
					characterLabel.fontWeight = FontWeight.Regular;
				}
			}
		}
	}
}
