﻿using System;
using System.Collections.Generic;
using Data;
using Data.CoreData;
using UnityEngine;

namespace DialBoard {
	/// <summary>
	/// Manager for the DialBoard. This script acts as the glue that holds together the two individual dial objects,
	/// holding their layouts and connecting to the facet.
	/// </summary>
	public class DialBoardManager : MonoBehaviour {
		public List<string> leftHandLayout;
		public List<string> rightHandLayout;

		public VD_DataFacet facet;
		
		public string currentValue;
		
		public enum Hand {
			Left, Right
		}

		public enum Heading {
			North = 0,
			Northeast = 1,
			East = 2,
			Southeast = 3,
			South = 4,
			Southwest = 5,
			West = 6,
			Northwest = 7
		}

		/// <summary>
		/// Method to retrieve the layouts for the passed hand. Returns the appropriate list of layouts for this hand.
		/// </summary>
		/// <param name="forHand">Which hand is requesting its layouts.</param>
		/// <returns>A list of layouts for this hand.</returns>
		public List<string> getLayouts(Hand forHand) {
			if (forHand == Hand.Left) {
				return leftHandLayout;
			}

			return rightHandLayout;
		}

		/// <summary>
		/// Add a character to the facet. Called by the individual dials when they are clicked down.
		/// </summary>
		/// <param name="hand">Which hand has gotten a click.</param>
		/// <param name="layout">Which layout number this hand is on.</param>
		/// <param name="heading">Which heading the user's finger is in.</param>
		/// <param name="upperCase">Whether or not upper case or caps lock was on.</param>
		public void addCharacter(Hand hand, int layout, Heading heading, bool upperCase) {
			List<string> targetList = hand == Hand.Left ? leftHandLayout : rightHandLayout;

			// Make sure the layout number isn't overflowing
			if (layout > targetList.Count) {
				return;
			}
			string targetLayout = targetList[layout - 1];

			// Perform upper-case conversion as needed
			if (upperCase) {
				currentValue += targetLayout.ToUpper()[(int) heading];
			} else {
				currentValue += targetLayout[(int) heading];
			}
			
			// Update the facet
			facet.UpdateData(new Text(currentValue));
		}

		/// <summary>
		/// Remove the last character from the string.
		/// </summary>
		public void Backspace() {
			// Make sure we even have space to delete
			if (currentValue.Length > 0) {
				currentValue = currentValue.Substring(0, currentValue.Length - 1);
				facet.UpdateData(new Text(currentValue));
			}
		}

		/// <summary>
		/// Event handler for when the text in the facet is pulled out. Resets the current value here to a blank string.
		/// </summary>
		public void textCleared() {
			currentValue = "";
		}
	}
}
