﻿using UnityEngine;
using UnityEngine.Events;

namespace Input {
    /// <summary>
    /// <c>VD_InputReceiver</c> should be attached to any component that would either be clicked on, or that could
    /// potentially be picked up, where getting input from the hand that's picking the object up matters. This script
    /// contains events for if the object is grabbed, dropped, or clicked, and provides a method to get the input of the
    /// hand that is currently holding the object. 
    /// </summary>
    public class VD_InputReceiver : MonoBehaviour {
        public UnityEvent grab;
        public UnityEvent drop;

        public UnityEvent click;

        private string givenId;
        private InputManager inputManager;

        public bool isActive;

        /// <summary>
        /// Called on start. Find the system level input manager.
        /// </summary>
        private void Start() {
            inputManager = FindObjectOfType<InputManager>();
        }

        /// <summary>
        /// Attempt to get the input state of the hand holding this object.
        /// </summary>
        /// <returns></returns>
        public VD_InputsState GetInput() {
            return inputManager.GetInput(givenId);
        }

        /// <summary>
        /// Called when the object is grabbed. Sets the input receiver as active and invokes the grab event.
        /// </summary>
        /// <param name="id">The grab ID.</param>
        public void Grabbed(string id) {
            givenId = id;
            isActive = true;
            grab.Invoke();
        }

        /// <summary>
        /// Called when the object is dropped. Registers the drop with the system level input manager, sets the input
        /// receiver as inactive, and invokes the drop event.
        /// </summary>
        public void Dropped() {
            inputManager.RegisterDrop(givenId);
            isActive = false;
            givenId = "";
            drop.Invoke();
        }
    }
}
