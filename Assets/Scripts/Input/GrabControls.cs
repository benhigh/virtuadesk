﻿using Data;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Input {
	/// <summary>
	/// <c>GrabControls</c> goes on both hands, and manages grabbing, dropping, and clicking objects and data around it.
	/// </summary>
	public class GrabControls : MonoBehaviour {
		public Grabbable grabbableCollidingWith;

		public InputManager.Hand hand;
		public bool holdingData;
		public VD_DataObject dataObjectHolding;
	
		private InputManager inputManager;

		/// <summary>
		/// Called on start. Get the system level input manager.
		/// </summary>
		private void Start() {
			inputManager = FindObjectOfType<InputManager>();
		}

		/// <summary>
		/// Try to grab an object. If we find an object we can grab, then register a grab with the system level input
		/// manager.
		/// </summary>
		public void Grab() {
			// Ask SteamVR if we're grabbing anything
			GameObject attachedObject = GetComponent<Hand>().currentAttachedObject;
			if (attachedObject != null) {
				// Now check if that is a VirtuaDesk grabbable object
				Grabbable grabbable = attachedObject.gameObject.GetComponent<Grabbable>();
				if (grabbable != null) {
					// Register the grab
					grabbableCollidingWith = grabbable;
			
					// Set the ID for the input receiver, if it's on the grabbable
					VD_InputReceiver receiver = grabbableCollidingWith.GetComponent<VD_InputReceiver>();
					if (receiver != null) {
						string id = inputManager.RegisterGrab(hand, grabbableCollidingWith.gameObject);
						receiver.Grabbed(id);
					}
			
					grabbableCollidingWith.Grabbed(this);
				}
			} else {
				// Check if we're hovering over any interactable - this would likely be a data facet
				Interactable interactable = GetComponent<Hand>().hoveringInteractable;
				if (interactable != null) {
					// Check if it's a data facet, and let it know we're here
					VD_DataFacet dataFacet = interactable.gameObject.GetComponent<VD_DataFacet>();
					if (dataFacet != null) {
						dataFacet.Grabbed(GetComponent<Hand>());
					}
				}
			}
		}

		/// <summary>
		/// Register a drop, informing the input receiver and the grabbable that the object is being dropped.
		/// </summary>
		public void Drop() {
			if (grabbableCollidingWith != null) {
				VD_InputReceiver receiver = grabbableCollidingWith.GetComponent<VD_InputReceiver>();
				if (receiver != null) {
					receiver.Dropped();
				}

				grabbableCollidingWith.Dropped(this);
				grabbableCollidingWith = null;
			}
		}

		/// <summary>
		/// Handle clicking. SteamVR really wants to pick objects up with the trigger, so we should make sure our hand
		/// doesn't have something in it. If it does, we can get the currently attached object in the hand. Otherwise,
		/// we can just get the interactable we're hovering over. From there, inform any attached input receiver of a
		/// click. Make sure we drop any objects we've picked up at the end.
		/// </summary>
		public void Click() {
			// Get whatever interactable we're hovering over. This may be a throwable, or it may not be.
			Interactable hoveringInteractable;
			if (GetComponent<Hand>().currentAttachedObject != null) {
				hoveringInteractable = GetComponent<Hand>().currentAttachedObject.GetComponent<Interactable>();
			} else {
				hoveringInteractable = GetComponent<Hand>().hoveringInteractable;
			}
		
			// Check if we're hovering an interactable
			if (hoveringInteractable != null) {
				// Check if the hovering interactable has an input receiver, and send it a click signal
				VD_InputReceiver inputReceiver = hoveringInteractable.GetComponent<VD_InputReceiver>();
				if (inputReceiver != null) {
					inputReceiver.click.Invoke();
				}
			}

			// SteamVR really wants to pick the object up, don't let it
			GetComponent<Hand>().DetachObject(GetComponent<Hand>().currentAttachedObject);
		}
	}
}
