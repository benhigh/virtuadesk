﻿namespace Input {
	/// <summary>
	/// Data class containing the trigger status. Contains the analog value of the trigger, and if it has been clicked.
	/// </summary>
	public class VD_InputTriggerState {
		private float value;
		private bool clicked;
		private bool clickedDown;
		private bool clickedUp;

		public VD_InputTriggerState(float value, bool clicked, bool clickedDown, bool clickedUp) {
			this.value = value;
			this.clicked = clicked;
			this.clickedDown = clickedDown;
			this.clickedUp = clickedUp;
		}

		public float Value => value;

		public bool Clicked => clicked;

		public bool ClickedDown => clickedDown;

		public bool ClickedUp => clickedUp;
	}
}