﻿using UnityEngine;

namespace Input {
	/// <summary>
	/// Data class containing the touchpad state. Contains the touchpad value, and if it has been clicked or touched.
	/// </summary>
	public class VD_InputTouchpadState {
		private Vector2 value;
		private bool clicked;
		private bool touched;
		private bool clickDown;
		private bool clickUp;
		private bool touchDown;
		private bool touchUp;

		public VD_InputTouchpadState(Vector2 value, bool clicked, bool touched, bool clickDown, bool clickUp, bool touchDown, bool touchUp) {
			this.value = value;
			this.clicked = clicked;
			this.touched = touched;
			this.clickDown = clickDown;
			this.clickUp = clickUp;
			this.touchDown = touchDown;
			this.touchUp = touchUp;
		}

		public Vector2 Value => value;

		public bool Clicked => clicked;

		public bool Touched => touched;

		public bool ClickDown => clickDown;

		public bool ClickUp => clickUp;

		public bool TouchDown => touchDown;

		public bool TouchUp => touchUp;
	}
}