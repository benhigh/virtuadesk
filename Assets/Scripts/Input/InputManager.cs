﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR;
using Random = System.Random;

namespace Input {
	/// <summary>
	/// System level input manager. This class reads input from SteamVR, manages the input delegation system, and
	/// communicates with <c>VD_InputReceiver</c>s.
	/// </summary>
	public class InputManager : MonoBehaviour {
		public enum Hand {
			Left, Right
		}

		private const int RandomIdLength = 16;
		private Dictionary<string, Tuple<Hand, GameObject>> holdMap;

		public GrabControls leftHand;
		public GrabControls rightHand;

		public SteamVR_ActionSet actionSet;

		public UniversalMenu leftUniversalMenu;
		public UniversalMenu rightUniversalMenu;

		private SteamVR_Action_Boolean gripAction;
		private SteamVR_Action_Boolean menuButton;
		
		private SteamVR_Action_Boolean triggerClick;
		private SteamVR_Action_Single triggerValue;
		
		private SteamVR_Action_Vector2 touchPadValue;
		private SteamVR_Action_Boolean touchPadClick;
		private SteamVR_Action_Boolean touchPadTouch;

		private SteamVR_Action_Boolean grabPinch;

		/// <summary>
		/// Called on start. Initialize our action set, and set all of our actions.
		/// </summary>
		private void Start() {
			actionSet.Activate();
			holdMap = new Dictionary<string, Tuple<Hand, GameObject>>();
			
			gripAction = SteamVR_Actions._default.GrabGrip;
			menuButton = SteamVR_Actions._default.MenuButton;
			triggerClick = SteamVR_Actions._default.TriggerClick;
			triggerValue = SteamVR_Actions._default.TriggerValue;
			touchPadValue = SteamVR_Actions._default.TouchpadValue;
			touchPadClick = SteamVR_Actions._default.TouchpadClick;
			touchPadTouch = SteamVR_Actions._default.TouchpadTouch;
			grabPinch = SteamVR_Actions._default.GrabPinch;
		}

		/// <summary>
		/// Called each frame. Listen for input information from SteamVR. If the grip is pressed down or released on
		/// either hand, inform the hand of that happening. Additionally, if the triggers are clicked down without the
		/// grips being down, inform the hands of that. Finally, manage the universal menu being invoked or dismissed.
		/// </summary>
		private void Update() {
			// Inform the left hand of any new grabs or drops
			if (gripAction.GetStateDown(SteamVR_Input_Sources.LeftHand)) {
				leftHand.Grab();
			} else if (gripAction.GetStateUp(SteamVR_Input_Sources.LeftHand)) {
				leftHand.Drop();
			}

			// Inform the right hand of any new grabs or drops
			if (gripAction.GetStateDown(SteamVR_Input_Sources.RightHand)) {
				rightHand.Grab();
			} else if (gripAction.GetStateUp(SteamVR_Input_Sources.RightHand)) {
				rightHand.Drop();
			}
			
			// Inform the left hand of any non-grabbing clicks
			if (!gripAction.GetState(SteamVR_Input_Sources.LeftHand) &&
			    grabPinch.GetStateDown(SteamVR_Input_Sources.LeftHand)) {
				leftHand.Click();
			}
			
			// Inform the right hand of any non-grabbing clicks
			if (!gripAction.GetState(SteamVR_Input_Sources.RightHand) &&
			    grabPinch.GetStateDown(SteamVR_Input_Sources.RightHand)) {
				rightHand.Click();
			}
			
			// Invoke or dismiss the left universal menu
			if (menuButton.GetStateDown(SteamVR_Input_Sources.LeftHand)) {
				if (leftUniversalMenu.gameObject.activeSelf) {
					rightUniversalMenu.gameObject.SetActive(false);
					rightUniversalMenu.menuActive = false;
					leftUniversalMenu.gameObject.SetActive(false);
					leftUniversalMenu.menuActive = false;
				} else {
					rightUniversalMenu.gameObject.SetActive(false);
					rightUniversalMenu.menuActive = false;
					leftUniversalMenu.gameObject.SetActive(true);
					leftUniversalMenu.menuActive = true;
					leftUniversalMenu.WakeUpMenu();
				}
			}
		
			// Invoke or dismiss the right universal menu
			if (menuButton.GetStateDown(SteamVR_Input_Sources.RightHand)) {
				if (rightUniversalMenu.gameObject.activeSelf) {
					leftUniversalMenu.gameObject.SetActive(false);
					leftUniversalMenu.menuActive = false;
					rightUniversalMenu.gameObject.SetActive(false);
					rightUniversalMenu.menuActive = false;
				} else {
					leftUniversalMenu.gameObject.SetActive(false);
					leftUniversalMenu.menuActive = false;
					rightUniversalMenu.gameObject.SetActive(true);
					rightUniversalMenu.menuActive = true;
					rightUniversalMenu.WakeUpMenu();
				}
			}
		}

		/// <summary>
		/// Register a grab. Generate a random grab ID, and add that to the grab map. If another hand is holding the
		/// object, instruct that hand to first drop the object.
		/// </summary>
		/// <param name="hand">Which hand is registering a grab.</param>
		/// <param name="obj">The object that is being grabbed.</param>
		/// <returns>The random grab ID.</returns>
		public String RegisterGrab(Hand hand, GameObject obj) {
			Random random = new Random();
			string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			string id = new string (Enumerable.Repeat(characters, RandomIdLength).Select(s => s[random.Next(s.Length)]).ToArray()) + "_" + (hand == Hand.Left ? "l" : "r");

			// If we're grabbing an item that's held in another hand, tell that hand to drop it first so control is passed to the new hand
			foreach (string key in holdMap.Keys) {
				if (holdMap[key].Item2 == obj) {
					if (holdMap[key].Item1 == Hand.Left) {
						leftHand.Drop();
					} else {
						rightHand.Drop();
					}
					break;
				}
			}
			
			holdMap.Add(id, new Tuple<Hand, GameObject>(hand, obj));
			return id;
		}

		/// <summary>
		/// Register a drop. Deregister a grab by removing it from the map. If the grab ID doesn't exist, log an error.
		/// </summary>
		/// <param name="id">The grab ID to deregister.</param>
		public void RegisterDrop(String id) {
			if (holdMap.ContainsKey(id)) {
				holdMap.Remove(id);
			} else {
				Debug.LogError("Attempt to drop object with ID " + id + " which is not registered.");
			}
		}

		/// <summary>
		/// Get the input for a hand. Using the grab ID, determine which hand we should get the input data for. If the
		/// grab ID is not registered, don't get any data.
		/// </summary>
		/// <param name="id">The registered grab ID, which will tell us which hand to get data for.</param>
		/// <returns>A <c>VD_InputsState</c> object with input data for the hand the grab ID is registered on, if it is
		/// registered. If not, then return a <c>VD_InputsState</c> object with null fields.</returns>
		public VD_InputsState GetInput(string id) {
			if (id == null) {
				return null;
			}
			
			// Verify ID
			if (!holdMap.ContainsKey(id)) {
				Debug.LogWarning("Object with ID " + id + " attempted to get input, but is not registered.");
				return new VD_InputsState(null, null);
			}
			
			// Get handedness
			SteamVR_Input_Sources source = holdMap[id].Item1 == Hand.Left
				? SteamVR_Input_Sources.LeftHand
				: SteamVR_Input_Sources.RightHand;
			
			// Get touch pad info
			VD_InputTouchpadState touchpadState = new VD_InputTouchpadState(
				touchPadValue.GetAxis(source),
				touchPadClick.GetState(source),
				touchPadTouch.GetState(source),
				touchPadClick.GetStateDown(source),
				touchPadClick.GetStateUp(source),
				touchPadTouch.GetStateDown(source),
				touchPadTouch.GetStateUp(source)
			);
			
			// Get trigger info
			VD_InputTriggerState triggerState = new VD_InputTriggerState(
				triggerValue.GetAxis(source),
				triggerClick.GetState(source),
				triggerClick.GetStateDown(source),
				triggerClick.GetStateUp(source)
			);
			
			return new VD_InputsState(triggerState, touchpadState);
		}
	}
}
